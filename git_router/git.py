from flask import Flask, Blueprint, render_template
from gitlab import Gitlab

token = 'Jz6VsZy7xjtaziUNU7R3'
con = Gitlab('http://127.0.0.1:8000',
                private_token=token)

gitlab = Blueprint('gitlab', __name__, url_prefix="/gitlab")


@gitlab.route('')
def index():
    users = con.users.list()
    projects = con.projects.list()

    return render_template('gitlab.html', users=users, projects=projects)