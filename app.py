#!/home/developer/python-devops/venv/bin/python3
from flask import Flask, render_template, request, session, redirect
from pdocker.bdocker import docker
from pjenkins.bjenkins import jenkins
from git_router.git import gitlab
from ldap3 import Connection, Server
from os import urandom
import logging

logging.basicConfig(
    filename='app.log',
    level = logging.DEBUG,
    format = "%(asctime)$ [%(levelname)$] %(name)$\n" +
    "[ %(funcName)$ ] [ %(filename)$ ] [ %(lineno)$ ] %(message)$",
    datefmt = "[ %d/%m/%Y %H:%M:%S ]"
)

app = Flask(__name__)
app.register_blueprint(docker)
app.register_blueprint(jenkins)
app.register_blueprint(gitlab)

@app.route("/", methods=['GET','POST'])
def index():
    if request.method == "GET":
        return render_template('index.html')
    elif request.method == "POST":
        auth = request.form
        session['auth'] = False
        server = Server('ldap://127.0.0.1:389')
        dn = "uid={},dc=dexter,dc=com,dc=br".format(auth['email'])
        con = Connection(server, user=dn, password=auth['password'])
        
        session['auth'] = con.bind()
        if session['auth'] == False:
            return redirect('/')

        logging.warning("login ou senha invalida")
        return redirect('/')

# @app.route("/")
# def index():
#     return render_template('index.html')
@app.route('/deslogar')
def deslogar():
    session['auth'] = False
    return redirect('/')

if __name__ == "__main__":
    app.secret_key = urandom(12)
    app.run(debug=True, port=5050)

