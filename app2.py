from flask import Flask, jsonify, request
from pymongo import MongoClient

try:
    con = MongoClient()
    db = con['teste']
except Exception as e:
    print(e)
    exit()

app = Flask(__name__)

def validar_json(json, method="GET"):
    if json:
        try:
            if json['nome'] and json['_id']:
                return True                   
            else:
                return False

            if method == "POST":
                if db.usuarios.find_one({"_id":int(json['id'])}):
                    return True                   
                else:
                    return False

        except KeyError:
            if method == "PUT":
                if json['nome']:
                    return True
            return False
    else:
        return False


@app.route('/usuarios/<string:busca>', methods=["GET"])
def get_user(busca):
    if busca.isnumeric():
        return jsonify(db.usuarios.find_one({"_id":int(busca)}))
    else:    
        return jsonify(list(db.usuarios.find({"nome": busca.lower().strip()})))


@app.route('/usuarios', methods=["GET","POST"])
def get_or_post_user():
    if request.method == "GET":
        return jsonify(list(db.usuarios.find()))
    else:    
        data = request.get_json()
        if isinstance(data, dict):
            if validar_json(data, method="POST"):
                data['_id'] = int(data['_id'])
                db.usuarios.insert(data)
                return jsonify({"status":True})
            else:
                return jsonify({"status":False})
        elif isinstance(data, list):
            for registro in data:
                if validar_json(registro, method="POST"):                    
                    data['_id'] = int(data['_id'])
                    db.usuarios.insert(data)
                else:    
                    return jsonify({"status":False})
            else:
                return jsonify({"status":True})   


# @app.route('/usuarios/<int:id>', methods=["GET"])
# def get_user(id):
#     usuario = db.usuarios.find_one({"_id":id})
#     return jsonify(usuario)

# @app.route('/usuarios/<string:nome>', methods=["GET"])
# def get_username(nome):
#     return jsonify(list(db.usuarios.find({'nome':nome})))    

# @app.route('/usuarios', methods=["GET"])
# def get_users():
#     return jsonify(list(db.usuarios.find()))


@app.route('/usuarios/<int:id>', methods=["PUT"])
def update_user(id):
    if request.method == "PUT":
        data = request.get_json()
        if validar_json(data, method="PUT"):
            db.usuarios.update({"_id":id},{"$set":data})
            return jsonify({"status":True})
        else:
            return jsonify({"status":False})
    elif request.method == 'DELETE':
        try:
            db.usuarios.remove({"_id":id})
            return jsonify({"status":True})
        except Exception:
            return jsonify({"status":False})
    

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, port=5000)